from distutils.core import setup

setup(
    name='CeleryMongoBackend',
    version='0.1.1',
    description='Updated celery backend that uses MongoDB',
    author='Arlena Derksen',
    author_email='arlena@hubsec.eu',
    classifiers=[
            'Development Status :: 4 - Beta',
            'Intended Audience :: Developers',
            'Programming Language :: Python :: 3.6'
    ],
    packages=['celery_mongo_backend'],
    install_requires=['vine', 'celery', 'kombu>=4.0.2,<5.0', 'pymongo>=3.4.0']
)
